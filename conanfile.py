import conans
import conans.errors


class ConanFileInst(conans.ConanFile):
    name = "dev-tools"
    version = "0.2"
    url = "https://gitlab.com/omicronns.main/conan-installers/dev-tools.git"
    author = 'omicronns@gmail.com'
    license = 'MIT'
    description = 'Installs and adds to DEV_TOOLS paths to dev tools'
    settings = "os", "arch"

    requires_template = (
        "cmake_installer/1.1@omicronns/tool",
        "arm-none-eabi-gcc_installer/0.2@omicronns/tool",
        "windows-build-tools_installer/0.1@omicronns/tool",
    )

    options_requires = {r.split("/")[0]: [True, False] for r in requires_template}
    default_options_requires = tuple([str(o) + "=True" for o in options_requires.keys()])

    options = {
        "uninstall": [True, False],
        **options_requires
    }

    default_options = (
        "uninstall=False",
    ) + default_options_requires

    build_policy = "missing"

    def requirements(self):
        if not self.options.uninstall:
            for opt in self.options.values.as_list():
                for req in self.requires_template:
                    if opt[0] in req and getattr(self.options, opt[0]):
                        if not ("windows" in req and self.settings.os == "Linux"):
                            self.requires(req)

    def build(self):
        if self.settings.os == "Linux":
            self.run("sed -z -i \"s/\\.dev_tools\\n//g\" ~/.bashrc")
        else:
            self.run("setx DEV_TOOLS \"\"")
        if not self.options.uninstall:
            if self.settings.os == "Linux":
                dev_tools = ":".join(self.env["PATH"])
                self.run("echo \"export PATH=PATH:$DEV_TOOLS\" > ~/.dev_tools")
                self.run("echo \"export DEV_TOOLS=%s\" > ~/.dev_tools" % dev_tools)
                self.run("echo \".dev_tools\" >> ~/.bashrc")
            else:
                dev_tools = ";".join(self.env["PATH"])
                self.run("setx DEV_TOOLS %s" % dev_tools)
